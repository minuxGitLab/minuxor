### Risk Constraint

$$
\min x'\Sigma x\leq \tau
$$

### Problem (CVX-book, Boyd, 5.29)

$$
\begin{align}
&\min & -3x_1^2+x_2^2+2x_3^2+2(x_1+x_2+x_3)\\
&s.t. & x_1^2+x_2^2+x_3^2=1
\end{align}
$$

This problem is `non-convex`, but the `strong duality` still holds. According to the `KKT` conditions, we could obtain all of the Lagrangian multipliers $\lambda$ satisfiying the conditions, then get the optimal solution.

Define $\lambda$ as the Lagrangian multiplier:
$$
L=-3x_1^2+x_2^2+2x_3^2+2(x_1+x_2+x_3)+\lambda(x_1^2+x_2^2+x_3^2-1)
$$
The KKT conditions are as follows:
$$
\left\{
\begin{align}
&x_1^2+x_2^2+x_3^2=1\\
&(-3+\lambda)x_1+1=0\\
&(1+\lambda)x_2+1=0\\
&(2+\lambda)x_3+1=0
\end{align}
\right.
$$
Eliminating the $x_1, x_2, x_3$:
$$
\frac{1}{(\lambda-3)^2}+\frac{1}{(1+\lambda)^2}+\frac{1}{(2+\lambda)^2}=1
$$
<img src="/Users/minux/Desktop/minux_programs/matlab/tutorials/ch0_solver_cvx/figures/kkt_lag.png" alt="kkt_lag" style="zoom:40%;" />

The solutions are: $\lambda_1=-3.15, \lambda_2=0.22, \lambda_3=1.89, \lambda_4=4.04$, and the optimal objective function is:
$$
f(x^*\mid \lambda^*=-4.04)=-4.70
$$
**Code Snippet 1 (yalmip+Gurobi)**:

```matlab
sdpvar x y z
obj=-3*x^2+y^2+2*z^2+2*(x+y+z);
F=[(x^2+y^2+z^2==1):'nonlinear-cons'];
lam=dual(F('nonlinear-cons'));
F=[x^2+y^2+z^2==1];
% saveampl(F, obj,'data/yalmip_model.mod');
ops=sdpsettings('solver', 'Gurobi', 'verbose', 1, 'savesolverinput', 1);
opt_diag = optimize(F, obj, ops);
fprintf("Yalmip call Gurobi solver:\n");
fprintf("the objective value is %.4f\n", value(obj));
disp("solution vector: ");
disp([value(x) value(y) value(z)]);
fprintf('the dual variable of this non-convex problem is %.4f\n', value(lam));
disp("---------------------------------------------");
```

**Code Snippet 2 (Gurobi Interface)**

```matlab
fprintf("Using Gurobi matlab interface\n");
names={'x', 'y', 'z'};
model.varnames=names;

% objective function
model.Q=sparse([-3 0 0; 0 1 0; 0 0 1]);
model.obj=[2; 2; 2];
model.modelsense='min';
model.vtype=['C'; 'C'; 'C']; % continuous
model.ub=[inf; inf; inf];
model.lb=[-inf; -inf; -inf];

% linear constraint
model.A=sparse(3, 3);
model.rhs=zeros(3, 1);
model.sense='=';

% non-linear constraint
model.quadcon.Qc=sparse([
    1 0 0;
    0 1 0;
    0 0 1;
]);
model.quadcon.q=sparse(3, 1);
model.quadcon.rhs=1;
model.quadcon.sense='=';
model.quadcon.name='non-linear';
params.nonconvex=2;
params.outputflag=1;
params.RLTCuts=2; % set the aggressive relative cuts
% params.qcpdual=1;
% model_rx=gurobi_relax(model);

gurobi_write(model, 'data/dual_nonlienar.mps');
% gurobi_write(model, 'data/dual_nonlienar_mip.mst');

opt=gurobi(model, params);

fprintf("the objective value is: %.4f\n", opt.objval);
for j=1:3
    fprintf("%s = %.4f\t", names{j}, opt.x(j));
end
fprintf("\n");
```

**Code Snippet 3 (fmincon)**

```matlab
func=@(x)(-3*x(1)^2+x(2)^2+x(3)^2+2*(x(1)+x(2)+x(3)));
x0=[1 0 0];
ops=optimoptions('fmincon', 'Display', 'iter');
[x, fval, exitflag, output, lambda, grad, hessian]=fmincon(func, x0, [], [], [], [], [], [], @mycon_5_29, ops);

fprintf("fmincon solver:\n");
fprintf('the objective value is %.4f\n', fval);
disp("solution vector: ");
disp(x);
fprintf('the dual variable of this non-convex problem is %.4f\n', lambda.eqnonlin);
disp("---------------------------------------------");
```

### Mixed Integer Quadratically Constrained Programming (MIQCP)

$$
\begin{align}
\min\quad & c'x+x'Q_0x\\
s.t.\quad &a_1'x+x'Q_1x\leq b_1\\
&\dots\\
&a_m'x+x'Q_mx\leq b_m\\
&l\leq x\leq u\\
&x_j\in\mathbb{Z}, \forall j\in\pmb{I}
\end{align}
$$

where $Q_k$ Are symmetric matrices. If all $Q_k$ are positive semi-definite, then `QCP` relaxtion is convex, which can be efficiently solved by Gurobi over 5.0.

**How about non-convex quadratic constraints or objective functions? **

General MINLP can be approximated as polynomial problem. Non-convex QP, QCP, MIQP, MIQCP: Prior Gurbobi versions could deal with the two types non-convexity: *Integer variables* and *SOS constraints*; Gurobi 9.0 can deal with a third type: *Bilinear constraints*. These non-convexities are treated by *Cutting planes* and *Branching*. 

**Translation of non-convex quadratic constraint constraints into bilinear constraints**
$$
\begin{align}
&f(x):=x_1^2-x_1x_2+2x_1x_3-x_2^2+3x_2x_3-x_3^2\\
&z_{11}=x_1^2, z_{12}=x_1x_2, z_{13}=x_1x_3, z_{22}=x_2^2, z_{23}=x_2x_3, z_{33}=x_3^2\\
&f(x)\to f(z)=z_{11}-z_{12}+2z_{13}-z_{22}+3z_{23}-z_{33}
\end{align}
$$
**Dealing wiht Bilinear Constraints**

general form: $a'x+dxy\leq b$ 

`LP Relaxation of Bilinear Constraints`

mixed product case: $-z+xy=0$. McCormick lower and upper envelopes:
$$
\begin{align}
&-z+l_xy+l_yx\leq l_xl_y\\
&-z+u_xy+u_yx\leq u_xu_y\\
&-z+u_xy+l_yx\geq u_xl_y\\
&-z+l_x+u_yx\geq l_xu_y
\end{align}
$$
*May lead to singular or ill-conditioned basis. In worst-case, simplex needs to start from scratch*.

`Locally valid cuts`

Add tighter McCormick relaxiation on top of weaker, more global one, to local node.

- Pros:
  1. Old simplex basis stays valid in all cases. more global McCormick constarints will likely become slack and basic.
  2. Should lead to fewer simplex iterations.
- Cons:
  1. Basis size changes all the time during solve. refactorization needed, complicated data management needed.
  2. Redundant more global McCormick constraints stay in LP. LP solver performs useless calculations in linear system solves.

`Spatial Branching`

**Branching variable selection**: branching on fractional integer variables as usual. If no fractional integer variable exists, select continuous variable in violated bilinear constraint.

*Gurobi variable selection rule*:

- sum of absolute bilinear constraint violations
- reduce McCormick volume as much as possible
- shadow costs of variable for linear constraints

**Branching value selection**: a convex combination of LP value and mid point of current domain.

*Avoid numerical pitfalls:*

- large branching values for unbounded variables
- tiny child domains if LP value is very close to bound
- deep dives in node selection

`Cutting Planes for Mixed Bilinear Programs`

*Special cuts for bilinear constraints:*

- RLT Cuts: Reformulation linearization technique. Multiply linear constraints with single variable, linearize resulting product terms. Very powerful for bilinear programs, also helps a bit for convex `MIQCP` and `MIP`.
- BQP Cuts: Facets from Boolean Quadratic Polytope.
- PSD Cuts: tangents of PSD cone defined by $Z=xx'$ relationship $Z-xx'\succeq 0$, but not implemented in Gurobi yet.

### Example of MIP

Solve the following integer-program with three binary variables:
$$
\min &x_0+x_1+x_2\\
s.t. & x_0, x_1, x_2 \in \{0, 1\}
$$
The feasible set is $S=\{0, 1\}^3$ and could be spliited into tow subsets according to the binary value of $x_0$: $S_0=\{x\in S, x_0=0\}$ and $S_1=\{x\in S, x_0=1\}$. Recursively, the tree of three levels could be constructed based on the binary values of $x_0, x_1, x_2$:

<img src="/Users/minux/Desktop/minux_programs/matlab/tutorials/ch0_solver_cvx/figures/Branch-and-Bound.png" alt="Branch-and-Bound" style="zoom:40%;" />

**Rules of Branch-and-Bound**

Relax the primal IP problem to a LP problem with continuous variables:
$$
\begin{align}
\min \quad &x_0+x_1+x_2\\
s.t. \quad &0\leq x_i\leq 1
\end{align}
$$

- If the LP problem is infeasible, then the IP problem is infeasible. If $x^*$ is the optimal solution of LP, and $x^*\in$ feasible integer domain of IP, $\mathbb{Z}_n$, then $x^*$ is the optimal solution of IP problem.

- Assuming the feasible domain of IP problem, $S$, can be decomposed of $n$ subsets, $S=S_1\cup S_2\dots, S_n$, and the following equality holds:
  $$
  v^i=\min\{c'x|x\in S_i\subseteq \mathbb{Z}_n, i=1, 2,\dots, n\}
  $$
  $\overline{v}^i$ is the upper bound of $v^i$, $\underline{v}^i$ is the lower bound of $v^k$. Hence, $\overline{v}=\min_i \overline{v}^i$ is the upper bound of $v^*$, $\underline{v}=\min_i \underline{v}^i$ Is the lower bound of $v^*$.

**Matlab Code**

```matlab
%% Guribi interface for solving simple example of IP
fprintf("Integer Programming with 3 binary variables:\n");
names={'x0', 'x1', 'x2'};
model.varnames=names;

% obj
model.obj=[1; 1; 1];
model.modelsense=['B'; 'B'; 'B']; % binary

% linear constraint
model.A=sparse(3, 3);
model.rhs=zeros(3, 1);
model.sense='=';

opt=gurobi(model);
fprintf("the objective value is: %.4f\n", opt.objval);
for j=1:3
    fprintf("%s = %.4f\t", names{j}, opt.x(j));
end
```

**Python Code**

```python
import gurobipy as gp
from gurobipy import GRB

def main():
    model=gp.Model('Simple_MIP')
    x0=model.addVar(vtype=GRB.BINARY, name='x0')
    x1=model.addVar(vtype=GRB.BINARY, name='x1')
    x2=model.addVar(vtype=GRB.BINARY, name='x2')

    model.setObjective(x0+x1+x2, GRB.MINIMIZE)
    model.optimize()

    for v in model.getVars():
        print(f'{v.varname} = {v.x}')


if __name__ == '__main__':
    main()
```

**Java Code**

```java
import gurobi.*;

public class MIP_simple_demo {
    public static void main(String[] args) {
        try {
            GRBEnv env = new GRBEnv(true);
            env.set("logFile", "data/mip_simple.log");
            env.start();

            GRBModel model = new GRBModel(env);
            GRBVar x0=model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "x0");
            GRBVar x1=model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "x1");
            GRBVar x2=model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "x2");

            // objective
            GRBLinExpr expr = new GRBLinExpr();
            expr.addTerm(1.0, x0); expr.addTerm(1.0, x1); expr.addTerm(1.0, x2);
            model.setObjective(expr, GRB.MINIMIZE);

            model.optimize();

            System.out.println(x0.get(GRB.StringAttr.VarName)+" "+ x0.get(GRB.DoubleAttr.X));
            System.out.println(x1.get(GRB.StringAttr.VarName)+" "+ x1.get(GRB.DoubleAttr.X));
            System.out.println(x2.get(GRB.StringAttr.VarName)+" "+ x2.get(GRB.DoubleAttr.X));
          
          	System.out.printf("Obj value = %.4f", model.get(GRB.DoubleAttr.ObjVal));

        }catch(GRBException e){
            System.out.println(e.getErrorCode()+" "+e.getMessage());
        }
    }
}
```

### Presolve

Presolve refers to a collection of problem reductions that are typically applied in advance of the start of the `branch-and-bound` procedure. These reductions are intended to reduce the size of the problem and to tighten its formulation.

### Simple example of Cutting Plane

Suppose the formulation includes the following constraint:
$$
6x_1+5x_2+7x_3+4x_4+5x_5\leq 15\\
x_i \in \{0, 1\}
$$
where $x_1\sim x_5$ are restricted to be binary. If the undesirable solution is $x_1=0, x_2=1, x_3=x_4=x_5=3/4$. Since $7+4+5=16>15$, then it is impossible that $x_3=x_4=x_5=1$, and the following new inequality is a valid addition to the given MIP: $x_3+x_4+x_5\leq 2$.

### Heuristics

The better the objective value of the incumbent, the more likely it is that the value of an LP relaxiation will exceed it and hence lead to a node being fathomed.

### Parallelism

Gurobi MIP solver runs in parallel. Different nodes in the MIP trees search can be processed independtly. Thus, models that explore large search trees can exploit cores quite effectively, while those that spend the majority of their runtime at root node are more constrained in their ability to utilize multiple cores.

### User Cuts and Lazy constraints

**User cuts** help tighten the relaxation of a MIP by removing fractional solutions. They are not required for the model, but they potentially help solve a MIP faster.

**Lazy constraints** are required for the model. For some models, it is helpful to designate some constraints as lazy when it is computationally faster to include them only when they are violated. Generally, they are used for models that contain a relatively large number of constraints, most of which are trivially satisfied. *By only including the constraints that are actually violated by solutions found during the branch-and-cut search, it is sometimes possible to find a proven solution while only adding a fraction of the full set of constraints*.

```python
'''
Interface:
Model.cbLazy(lhs, sense, rhs)
Arguments:
lhs: left-hand side for new lazy constraint. Var or LinExpr
sense: Sense for new lazy constraint. GRB.LESS_EQUAL, GRB.EQUAL, GRB.GREATER_EQUAL
rhs: right-hand side for new lazy constraint. constant, Var, or LinExpr
'''
def _callback(model, where):
  if where==GRB.Callback.MIPSOL:
  	sol=model.cbGetSolution([model._vars[0], model._vars[1]])
    if sol[0] + sol[1]>1.1:
      model.cbLazy(model._vars[0]+model._vars[1] <= 1)
      
model._vars=model.getVars()
model.optimize(_callback)
```

### Callbacks

If you call routine `GRBterminate` from within a callback, the optimizer will terminate at the earliest convenient point. Routine `GRBcbsolution` allows you to inject a feasible solution (or partial solution) during the solution of a MIP model. Routines `GRBcbcut` and `GRBLazy` allow you to add *cutting planes* and *lazy constraints* during a MIP optimization, respectively.

```java
if(where==GRB.CB_MIP){
                // mip callback
                double node_cnt=getDoubleInfo(GRB.CB_MIP_NODCNT);
                double obj_bst=getDoubleInfo(GRB.CB_MIP_OBJBST); // current best objective
                double obj_bnd=getDoubleInfo(GRB.CB_MIP_OBJBND); // current best objective bound
                int sol_cnt=getIntInfo(GRB.CB_MIP_SOLCNT); // current count of feasible solutions found
                if(node_cnt-last_node>=100){
                    int act_nodes=(int) getDoubleInfo(GRB.CB_MIP_NODLFT); // current unexplored node count
                    int it_cnt=(int) getDoubleInfo(GRB.CB_MIP_ITRCNT); // current simplex iteration count
                    int cut_cnt=getIntInfo(GRB.CB_MIP_CUTCNT); // current count of cutting planes applied
                    System.out.println(node_cnt+" "+act_nodes+" "+it_cnt+" "+obj_bst+" "+obj_bnd+" "+sol_cnt+" "+cut_cnt);
                }
                if(Math.abs(obj_bst-obj_bnd)<0.1*(1.0+Math.abs(obj_bst))){
                    System.out.println("Stop early - 10% gap achieved"); abort();
                }
                if(node_cnt>=10000 && sol_cnt>0){
                    System.out.println("Stop early - 10000 nodes explored"); abort();
                }
}
```

### Relax functions

1. `gurobi_iis()`: compute an irreducible inconsistent subsystem. An $IIS$ is a subset of the constraints and variable bounds with the following properties:

- it is still infeasible
- if a single constraint or bound is removed, the subsystem becomes feasible

```matlab
model=gurobi_read('data/mymodel.mps');
iis=gurobi_iis(model);
```

*Returned values*:

`minimal`: A logical scalar that indicates whether the computed IIS is minimal. It will normally be true, but it may be false if the IIS computation was stopped early.

`Arows`: A logical vector that indicateds whether a linear constraint appears in the computed IIS.

2. `gurobi_feasrelax(model, relaxobjtype, minrelax, penalities, params)`: The feasibility relaxiation is a model that, when solved, minimizes the amount by which the solution violates the bounds and linear constraints of the original model. Providing a *penalty* to associate with relaxing each individual bound or constraint.

*Arguments*

`relaxobjtype`: impose penalties on violations. `relaxobjtype=0`,  minimize the sum of the weighted magnitudes of the bound and constraint violations. `relaxobjtype=1`, minimize the weighted sum of the squares of the bound and constraint violations. `relaxobjtype=2`, minimize the weighted count of bound and constraint violations. In all cases, the weights are taken from `penalties.lb`, `penalties.ub`, and `penalties.rhs`, `Inf` indicates the corresponding bound or constraint cannot be relaxed.

`minrelax`: boolean and controls the type of feasibility relaxation that is created. If `minrelax=false`, optimizing the returned model gives a solution that minimizes the cost of the violation. If `minrelax=true`, optimizing the returned model finds a solution that minimizes the original objective,  but only from among those solutions that minimize the cost of the violation.

*Usage*

```matlab
model=gurobi_read('model.mps');
penalities.lb=ones(length(model.lb), 1);
penalities.ub=ones(length(model.ub), 1);
penalities.rhs=ones(length(model.rhs), 1);
fea_res=gurobi_feasrelax(mode, 0, false, penalities);
```

3. `computeIIS()`:  An `IIS` is a subset of the constraints and variable bounds with the following properties, if a single constraint or bound is removed, the subsystem becomes feasible. This method can be used to compute `IIS` for both continuous and MIP models.

*Usage*

```matlab
model.computeIIS();
model.write(model.ilp);
```





