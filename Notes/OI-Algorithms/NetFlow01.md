## 流网络

源点(source, s)和汇点(sink, t), `G=(V, E)`

可行流`f`: 1.**容量限制** $0\leq f(u, v)\leq c(u, v)$; 2.**流量守恒** $\sum_{(v, x)\in E}f(v, x)=\sum_{(x, v)\in E}f(x, v)$.

净流量:$|f|=\sum_{(s, v)\in E}f(s, v)-\sum_{(v, s)\in E}f(v, s)$.

最大流:最大可行流

残留网络:$V_f=V, E_f=E$包含$E$和$E$中的反向边(`antiparallel edges`); 容量:
$$
c'(u, v)=
\left\{
\begin{align}
&c(u, v)-f(u, v) & (u, v)\in E\\
&f(u, v) & (v, u)\in E
\end{align}
\right.
$$
$f+f'$也是$G$的一个可行流，且$|f+f'|=|f|+|f'|$.

> The capcity constraint simply says that the flow from one vertex to another must be nonnegative and must not exceed the given capacity. The flow-conservation property says that the total flow into a vertex other than the source or sink must equal the total flow out of that vertex, informally, *flow in equals flow out*.

增广路径: 如果对于流`f`不存在增广路径，那么`f`是最大流

割: $G=(V, E), V=S\bigcup T, S\bigcap T=\empty, s\in S, t\in T$.

割的容量: $c(S, T)=\sum_{u\in S}\sum_{v\in T} c(u, v)$

割的流量: $f(S, T)=\sum_{u\in S}\sum_{v\in T}f(u, v)-\sum_{u\in T}\sum_{v\in S}f(v, u)$

## 最大流最小割定理

1. 流`f`是最大流
2. 流`f`的残留网络中不存在增广路
3. 存在某个割$[S, T], |f|=c(S, T)$

**FORD-FULKSON-Method**

1. 找`augmenting path`
2. 更新`residual network`

Initialize flow $f\to 0$

while there exists an augmenting path $p$ in the residual network $G_f$

​		augement flow $f$ along $p$

return $f$

### Dinic算法

1.`bfs`建立分层图

2.`dfs`找出所有能增广的路径

原问题的解是否可以对应到可行流