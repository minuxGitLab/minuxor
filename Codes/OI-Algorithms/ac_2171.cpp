#include<cstring>
#include<iostream>
#include<algorithm>

using namespace std;

const int N = 1010, M=20010, INF=1e8;

int n, m, S, T;
int h[N], e[M], f[M], ne[M], idx;
// d数组记录了从S到当前节点最小的流量
int q[N], d[N], pre[N];
bool st[N];

/*
EK算法求最大流
*/

void add(int a, int b, int c){
    e[idx]=b, f[idx]=c, ne[idx]=h[a], h[a]=idx++;
    e[idx]=a, f[idx]=0, ne[idx]=h[b], h[b]=idx++; // anti parallel edge
}


bool bfs(){
    int hh=0, tt=0;
    memset(st, false, sizeof st);
    q[0]=S, st[S]=true, d[S]=INF;
    while(hh<=tt){
        int t=q[hh++];
        for(int i=h[t]; ~i; i=ne[i]){
            int ver = e[i];
            if(!st[ver] && f[i]){ // 如果当前点没有被访问过并且具有流量
                st[ver]=true;
                d[ver] = min(d[t], f[i]); // slack
                pre[ver]=i; // 记录前驱边
                if(ver==T) return true;
                q[++tt]=ver;
            }
        }
    }
    return false;
}

int EK(){
    int r=0;
    while(bfs()){
        r+=d[T]; // 当前流量
        for(int i=T; i!=S; i=e[pre[i]^1]){ // 找反向边
            f[pre[i]]-=d[T], f[pre[i]^1]+=d[T]; // update
        }
    }
    return r;
}

int main(){
    scanf("%d%d%d%d", &n, &m, &S, &T);
    memset(h, -1, sizeof h);
    while(m--){
        int a, b, c;
        scanf("%d%d%d", &a, &b, &c);
        add(a, b, c);
    }

    printf("%d\n", EK());
    return 0;    
}