#include <iostream>
#include <cstring>
#include <algorithm>

using namespace std;

const int N=10005, M=200005, INF=1e8;

int n, m, S, T;
int h[N], e[M], f[M], ne[M], idx; // f表示当前残留网络的容量
int q[N], d[N], cur[N]; // d表示层数


void add(int a, int b, int c){
    e[idx]=b, f[idx]=c, ne[idx]=h[a], h[a]=idx++;
    e[idx]=a, f[idx]=0, ne[idx]=h[b], h[b]=idx++;
}

bool bfs(){
    // 1.判断是否存在增广路
    // 2.如果存在增广路则返回分层图
    int hh=0, tt=0;
    memset(d, -1, sizeof d);
    q[0]=S, d[S]=0, cur[S]=h[S]; // 加入当前弧优化
    while(hh<=tt){
        int t = q[hh++];
        for(int i=h[t]; ~i; i=ne[i]){
            int ver = e[i];
            if(d[ver]==-1 && f[i]){
                d[ver]=d[t]+1;
                cur[ver]=h[ver];
                if(ver==T) return true; // 找到增广路
                q[++tt] = ver;
            }
        }
    }
    return false;
}

int find(int u, int limit){
    // 从S点到u点的流量上限
    if(u==T) return limit;
    int flow=0; // 从u点开始向后流的最大流量
    for(int i=cur[u]; ~i && flow<limit; i=ne[i]){
        // 考虑当前弧优化
        cur[u] = i;
        int ver = e[i];
        if(d[ver] == d[u]+1 && f[i]){
            int t = find(ver, min(f[i], limit-flow));
            if(!t) d[ver]=-1; // 路径不可用
            f[i]-=t, f[i^1]+=t, flow+=t;
        }
    }
    return flow;
}

int dinic(){
    int r=0, flow;
    while(bfs()){
        while(flow=find(S, INF)) r+=flow; // 将所有能够增广路径的流量进行累加
    }
    return r;
}

int main(){
    scanf("%d%d%d%d", &n, &m, &S, &T);
    memset(h, -1, sizeof h);
    while(m--){
        int a, b, c;
        scanf("%d%d%d", &a, &b, &c);
        add(a, b, c);
    }

    printf("%d\n", dinic());
    return 0;
}