#include <iostream>
#include <cstring>
#include <algorithm>

using namespace std;
/*
1. 0~n+1号点之间是否联通，使用Union-Find Set
2. 如果存在解，判断用day天，能否可以将所有人运送
3. Using layered graphs
kth day is the kth level.
In each level, set space station as the node, ship as the edge.
*/

const int N=1101*50+10, M=(N+1100+20*1101)+10, INF=1e8;
int n, m, k, S, T;
int h[N], e[M], f[M], ne[M], idx;
int q[N], d[N], cur[N];

struct Ship{
    int h, r, id[30];
}ships[30];

int p[30]; // union-find set

int find(int x){
    if(p[x]!=x) p[x]=find(p[x]);
    return p[x];
}

int get(int i, int day){
    return day*(n+2)+i;
}

void add(int a, int b, int c){
    e[idx]=b, f[idx]=c, ne[idx]=h[a], h[a]=idx++;
    e[idx]=a, f[idx]=0, ne[idx]=h[b], h[b]=idx++;
}

bool bfs(){
    int hh=0, tt=0;
    memset(d, -1, sizeof d);
    q[0]=S, d[S]=0, cur[S]=h[S];
    while(hh<=tt){
        int t=q[hh++];
        for(int i=h[t]; ~i; i=ne[i]){
            int ver=e[i];
            if(d[ver]==-1 && f[i]){
                d[ver]=d[t]+1;
                cur[ver]=h[ver];
                if(ver==T) return true;
                q[++tt]=ver;
            }
        }
    }
    return false;
}

int find(int u, int limit){
    if(u==T) return limit;
    int flow=0;
    for(int i=cur[u]; ~i && flow<limit; i=ne[i]){
        cur[u]=i;
        int ver=e[i];
        if(d[ver]==d[u]+1 && f[i]){
            int t=find(ver, min(f[i], limit-flow));
            if(!t) d[ver]=-1;
            f[i]-=t, f[i^1]+=t, flow+=t;
        }
    }
    return flow;
}


int dinic(){
    int r=0, flow;
    while(bfs()) while(flow=find(S, INF)) r+=flow;
    return r;
}

int main(){
    cin>>n>>m>>k;
    S=N-2, T=N-1;
    memset(h, -1, sizeof h);

    for(int i=0; i<30; ++i) p[i]=i;
    for(int i=0; i<m; ++i){
        int a, b;
        cin>>a>>b;
        ships[i]={a, b};
        for(int j=0; j<b; ++j){
            int id;
            cin>>id;
            if(id==-1) id=n+1;
            ships[i].id[j]=id;
            if(j){
                int x=ships[i].id[j-1];
                p[find(x)]=find(id);
            }
        }
    }
    if(find(0)!=find(n+1)) puts("0");
    else{
        add(S, get(0, 0), k);
        add(get(n+1, 0), T, INF);
        int day=1, ans=0;
        while(1){
            add(get(n+1, day), T, INF);
            for(int i=0; i<=n+1; ++i)
                add(get(i, day-1), get(i, day), INF);

            for(int i=0; i<m; ++i){
                int r=ships[i].r;
                int a=ships[i].id[(day-1)%r], b=ships[i].id[day%r];
                add(get(a, day-1), get(b, day), ships[i].h);
            }
            ans+=dinic();
            if(ans>=k) break;
            ++day;
            
        }
        cout<<day<<endl;
    }
    return 0;
}



