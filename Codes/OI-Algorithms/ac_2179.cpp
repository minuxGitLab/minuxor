#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;
/*
二分图的多重匹配问题
问题本质: 最大流=总人数
*/

const int N=430, M=(150*270+N)*2, INF=1e8;
int n, m, S, T;
int h[N], e[M], f[M], ne[M], idx;
int q[N], d[N], cur[N];

void add(int a, int b, int c){
    e[idx]=b, f[idx]=c, ne[idx]=h[a], h[a]=idx++;
    e[idx]=a, f[idx]=0, ne[idx]=h[b], h[b]=idx++;
}

bool bfs(){
    int hh=0, tt=0;
    memset(d, -1, sizeof d);
    q[0]=S, d[S]=0, cur[S]=h[S];
    while(hh<=tt){
        int t=q[hh++];
        for(int i=h[t]; ~i; i=ne[i]){
            int ver = e[i];
            if(d[ver]==-1 && f[i]){
                d[ver]=d[t]+1;
                cur[ver]=h[ver];
                if(ver==T) return true;
                q[++tt]=ver;
            }
        }
    }
    return false;
}

int find(int u, int limit){
    if(u==T) return limit;
    int flow=0;
    for(int i=cur[u]; ~i && flow<limit; i=ne[i]){
        cur[u]=i;
        int ver = e[i];
        if(d[ver]==d[u]+1 && f[i]){
            int t = find(ver, min(f[i], limit-flow));
            if(!t) d[ver]=-1;
            f[i]-=t, f[i^1]+=t, flow+=t;
        }
    }
    return flow;
}

int dinic(){
    int r = 0, flow;
    while(bfs()) while(flow=find(S, INF)) r+=flow;
    return r;
}

int main(){
    scanf("%d%d", &m, &n);
    S=0, T=n+m+1;
    memset(h, -1, sizeof h);
    int tot=0; // 总人数

    for(int i=1; i<=m; ++i){
        int c;
        scanf("%d", &c);
        add(S, i, c);
        tot+=c;
    }

    for(int i=1; i<=n; ++i){ // 左半边桌子及编号
        int c;
        scanf("%d", &c);
        add(m+i, T, c);
    }

    for(int i=1; i<=m; ++i){ // 右半边桌子及编号
        for(int j=1; j<=n; ++j)
            add(i, m+j, 1);
    }

    if(dinic() != tot) puts("0");
    else{
        puts("1");
        // 输出方案
        for(int i=1; i<=m; ++i){
            for(int j=h[i]; ~j; j=ne[j]){
                if(e[j]>m && e[j]<=m+n && !f[j])
                    printf("%d ", e[j]-m);
            }
            puts("");
        }
    }

    return 0;
}