/*
二分法
假设在最大长度为M的情况下，能否走T次
无向边=流网络中建立两条边
*/
#include <iostream>
#include <cstring>
#include <algorithm>

using namespace std;

const int N=205, M=80005, INF=1e8;

int n, m, K, S, T;
int h[N], e[M], f[M], w[M], ne[M], idx;
int q[N], d[N], cur[N];

void add(int a, int b, int c){
    e[idx]=b, w[idx]=c, ne[idx]=h[a], h[a]=idx++;
    e[idx]=a, w[idx]=c, ne[idx]=h[b], h[b]=idx++;
}


bool bfs(){
    int hh=0, tt=0;
    memset(d, -1, sizeof d);
    q[0]=S, d[S]=0, cur[S]=h[S];
    while(hh<=tt){
        int t=q[hh++];
        for(int i=h[t]; ~i; i=ne[i]){
            int ver=e[i];
            if(d[ver]==-1 && f[i]){
                d[ver]=d[t]+1;
                cur[ver]=h[ver];
                if(ver==T) return true;
                q[++tt]=ver;
            }
        }
    }
    return false;
}

int find(int u, int limit){
    if(u==T) return limit;
    int flow=0;
    for(int i=cur[u]; ~i && flow<limit; i=ne[i]){
        cur[u]=i;
        int ver=e[i];
        if(d[ver]==d[u]+1 && f[i]){
            int t=find(ver, min(f[i], limit-flow));
            if(!t) d[ver]=-1;
            f[i]-=t, f[i^1]+=t, flow+=t;
        }
    }
    return flow;
}

int dinic(){
    int r=0, flow;
    while(bfs()) while(flow=find(S, INF)) r+=flow;
    return r;
}


bool chk(int mid){
    for(int i=0; i<idx; ++i){
        if(w[i]>mid) f[i]=0; // 删除改路径
        else f[i]=1;
    }
    return dinic()>=K;
}

int main(){
    scanf("%d%d%d", &n, &m, &K); // n个农场，m个双向路，往返次数为K
    S=1, T=n;
    memset(h, -1, sizeof h);

    while(m--){
        int a, b, c;
        cin>>a>>b>>c;
        add(a, b, c);
    }

    // binary search
    int l=1, r=1e6;
    while(l<r){
        int mid=(l+r)/2;
        if(chk(mid)) r=mid;
        else l=mid+1;
    }
    cout<<l<<endl;
    return 0;
}

